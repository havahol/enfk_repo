mrstModule add ad-core ad-blackoil deckformat diagnostics mrst-gui ad-props incomp optimization spe10


setupModel2D % Running this model with just one injector and 1 producer
% Create model-object of class TwoPhaseOilWaterModel
%model_ref  = TwoPhaseOilWaterModel(G, rock, fluid);
 model_ref  = GenericBlackOilModel(G, rock, fluid);
model_ref.gas=false;
% Set initial state and run simulation:
state0 = initResSol(G, 200*barsa, [0.2, 0.8]);

%% Compute flow solution and diagnostic quantities
% As usual, we only solve a single-phase flow problem to get the flow field
% needed for the diagnostics
rS = initState(G, W, 0);
T  = computeTrans(G, rock);
rS = incompTPFA(rS, G, T, fluid, 'wells', W);
D = computeTOFandTracer(rS, G, rock, 'wells', W);

%% Well pairs
% Having established the injection and tracer partitions, we can identify
% well pairs and compute the pore volumes of the regions that can be
% associated with each well pair.
fig2=figure;
WP = computeWellPairs(rS, G, rock, W, D);
pie(WP.vols, ones(size(WP.vols)));
legend(WP.pairs,'location','Best');

figure; clf
p = compressPartition(D.ipart + D.ppart*max(D.ipart))-1;
plotCellData(G,p,p>=0,'EdgeColor','k','EdgeAlpha',.05);
plotWell(G,W,'height',2,'FontSize',14); axis tight;
set(gca,'dataaspect',[1 1 0.06]), view(-60,15); axis off

%% Calculate average permeability for every well pair
perm_wp = ones(1,4);
idx_wp = 1;
% I1_P1 I1_P2 I2_P1 I2_P2
for ni = 1:2
    for np = 1:2
        % The mean perm is calculated according to geometry properties.
        perm_wp(idx_wp) = mean(rock.perm(D.itracer(:,ni) .* D.ptracer(:,np) >0.01)); 
        idx_wp = idx_wp + 1;
    end
    
end

% I1_P1 I1_P2 I2_P1 I2_P2
idx_wp = 1;
for ni = 1:2
    for np = 1:2
        % Mean perm for every well pair according to the geometry
        % sum (perm .* poreVolume .* (itracer .* ptracer)./ sum(poreVolume.* (itracer .* ptracer)))
        perm_wp(idx_wp) = sum(rock.perm(:,1) .* poreVolume(G, rock) .* (D.itracer(:,ni) .* D.ptracer(:,np)) ./sum(poreVolume(G, rock).* (D.itracer(:,ni) .* D.ptracer(:,np))));
        idx_wp = idx_wp + 1;
    end
end

% The calculation method of flux-based mean permeability
dist_W = ones(4,1);
dist_W(1) = norm(G.cells.centroids(W(1).cells,:,:)-G.cells.centroids(W(3).cells,:,:));
dist_W(2) = norm(G.cells.centroids(W(2).cells,:,:)-G.cells.centroids(W(3).cells,:,:));
dist_W(3) = norm(G.cells.centroids(W(1).cells,:,:)-G.cells.centroids(W(4).cells,:,:));
dist_W(4) = norm(G.cells.centroids(W(1).cells,:,:)-G.cells.centroids(W(4).cells,:,:));

DP_W = ones(4,1);
DP_W(1) = rS.wellSol(1).pressure-rS.wellSol(3).pressure;
DP_W(2) = rS.wellSol(2).pressure-rS.wellSol(3).pressure;
DP_W(3) = rS.wellSol(1).pressure-rS.wellSol(4).pressure;
DP_W(4) = rS.wellSol(2).pressure-rS.wellSol(4).pressure;

K_mas = ones(4,1);

% The unit needs to be adjusted according to the field unit.
% I1_P1 I2_P1 I1_P2 I2_P2
for i = 1 : 2  % K_mean belong to I1_P1 I2_P1
    K_mas(i) = 0.01*abs(WP.prod(1).alloc(i)*(dist_W(i))^2/ (DP_W(i) * WP.vols(i))); % The unit transformation
end

for i = 1 : 2  % K_mean belong to I1_P2 I2_P2
    K_mas(i+2) = 0.01*abs(WP.prod(2).alloc(i)*(dist_W(i+2))^2/ (DP_W(i+2) * WP.vols(i+2))); 
end    


% Volumes associated with each well pair
vols = D.itracer'*bsxfun(@times, poreVolume(G, rock), D.ptracer);
WP.vols = vols(:)';


% The pore volume for each well pair 
L = 310;
poro_mean = ones(4,1);
for i = 1 : numel(vols)
    poro_mean(i) = vols(i)/ (L*L/5*L/5/4*meter^3);
end

%%
[wellSols_ref, states] = simulateScheduleAD(state0, model_ref, schedule);
ts = [ [1 1 3 5 5 10 10 10 15 15 15 15 15 15 15]'*day; ...
                   repmat(150/10, 10, 1)*day; ...
                   repmat(150/6, 6, 1)*day; ...
                   repmat(150/6, 6, 1)*day];
plotWellSols(wellSols_ref,cumsum(ts))
 states_comp = ['states_comp_reference.mat'];
 save( states_comp,'states');  