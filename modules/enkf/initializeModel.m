%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Initialization of models before forward simulation.
%
% PARAMETERS:
%
% RETURNS:
%   dynamic state and structure arrays for the truth and for all ensemble models
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

try
    require incomp
catch %#ok<CTCH>
    mrstModule add incomp
end

% We use MATLAB(R)'s built-in <matlab:doc('mldivide') |mldivide|>
% ("backslash") linear solver software to resolve all systems of linear
% equations that arise in the various discretzations.  In many cases, a
% multigrid solver such as Notay's AGMG package may be prefereble.  We
% refer the reader to Notay's home page at
% http://homepages.ulb.ac.be/~ynotay/AGMG/ for additional details on this
% package.
linsolve_p = @mldivide;  % Pressure
linsolve_t = @mldivide;  % Transport (implicit)

% structural update
if t == 0 && (iter > 1 || iu > 1)
  if length(nstat) > 4
    load(EnkfFile,'U');
    G = U(sum(nstat(1:4))+1:sum(nstat),:);
    clear U
  end
end

% set up matlab structures for truth (l=1) and ensemble members (l=2)
for l = 1 : 2

 if l == 1
   ne = 0;
 else
   ne = nmembers;
 end

 for j = min(1,ne) : ne

   % construct the model
   if j == 0 || ~isempty(G)
     [g, wells, fluid, rock, p0, s0, phi, K, G] = getModel(workdir,modelname,nmembers,j,G);
     imap = g.cells.indexMap;
     if j == 0
        Grid = g; 
     end
     if j > 0 && ~isempty(G)
        gridE{j};
     end
   end
 
   % assign rock properties
   if j > 0
     if iter == 1 && iu == 1
       rockE{j}.perm = [K(:,j) K(:,j) K(:,j)/10];
       rockE{j}.poro = phi(:,j);
     else
       load(EnkfFile,'U');
       rockE{j}.perm = [U(1:nstat(1),j) U(1:nstat(1),j) U(1:nstat(1),j)/10];
       rockE{j}.poro = U(nstat(1)+1:sum(nstat(1:2)),j);
       rSolE{j}.pressure = U(sum(nstat(1:2))+1:sum(nstat(1:3)),j);
       rSolE{j}.s = U(sum(nstat(1:3))+1:sum(nstat(1:4)),j);
       clear U
     end
   end
 
   % define wells, control targets and compute grid connections
   W = [];
   g = Grid; if j > 0 && ~isempty(G), g = gridE{j}; end
   r = rock; if j > 0, r = rockE{j}; end
   for iw = 1 : length(wells)
     X = wells{iw}{1}; Y = wells{iw}{2}; Z = 1:g.cartDims(3);
     type = 'rate'; val = wells{iw}{3}; 
     if wells{iw}{3}<0, type = 'bhp'; val = wells{iw}{4}; end
     radius = wells{iw}{5}; comp = wells{iw}{6}; name = wells{iw}{7};
     W = verticalWell(W, g, r, X, Y, Z, 'Type', type, ...
         'Val', val, 'Radius', radius, 'Comp_i', comp, 'name', name);
   end
   if j == 0
     well = W;
   else
     wellE{j} = W;
   end
   
   % TO DO: should all solver initialization be moved to the simulationModel
   % instead?
   
   if t == 0
     if j == 0

         % grid transmissibilities
         trans = computeTrans(Grid, rock);
         
         % initial state
         rSol = initState(Grid, well, p0, [0.2,0.8]);
                 
         % solve initial pressure
         rSol = incompTPFA(rSol, Grid, trans, fluid, 'wells', well, ...
                     'LinSolve', linsolve_p);
         
     else
         
         grd = Grid; if ~isempty(G), grd = gridE{j}; end
         % grid transmissibilities
         transE{j} = computeTrans(grd, rockE{j});
         
         % initial state
         rSolE{j} = initState(grd, wellE{j}, p0, [0.2,0.8]);
         
         % solve initial pressure
         rSolE{j} = incompTPFA(rSolE{j}, grd, transE{j}, fluid, 'wells', wellE{j}, ...
                     'LinSolve', linsolve_p);

     end
   end

 end

end

clear ne
       