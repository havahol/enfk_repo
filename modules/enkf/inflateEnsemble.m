%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   User specified or adaptive inflation.
%
% PARAMETERS:
%
% RETURNS:
%   Inflated ensemble U
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

if inflation == 1
    % adaptive inflation
    B1 = A(sum(nstat)+1:end,:);
    B2 = U(sum(nstat)+1:end,:);

    std1 = std(reshape(B1,numel(B1,1)));
    std2 = std(reshape(B2,numel(B2,1)));

    rho = std1/std2;
else
    % user-specified inflation factor
    rho = inflation;
end

Um = mean(U,2);
U = U - repmat(Um,1,nmembers);
U = rho * U;
U = U + repmat(Um,1,nmembers);

% U(sum(nstat)+1:end,:) = [];
