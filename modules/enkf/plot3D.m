%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%% input
workDir = [ROOTDIR 'ENKF/'];
model   = 'testGrid1';
time    = 16425;   
iter    = 1;

%% load grid
load([workDir model '.mat']);
ng   = size(K,1);
ns   = numel(G);
nstat = [ng ng ng ng]; if ns > 0, nstat = [nstat ns]; end

% axis equal
% figure
% plotCellData(grid, convertTo(rock.perm(:,1),milli*darcy), 'EdgeColor', 'k', 'FaceAlpha', 1.0);
% plotWell(grid, well, 'height', 75, 'color', 'k');
% axis tight off, view(15,60), h=colorbar('horiz');
% zoom(2.5), title('permeability [mD]');

%% load experiment settings
load([workDir 'summary_' num2str(time) '_iter' num2str(iter) '.mat']);

for i = 1: length(settings)
    itransform{i} = settings(i).transform;
end
% for i = 1 : length(itransform)
%      itransform{i}(1) = -1 * itransform{i}(1);  
% end
ne   = settings.nmembers;

%% construct states
load([workDir 'states_' num2str(time) '_' num2str(iter) '.mat']);

% states (A0 - before updating; U - after updating)
grd = U; % - A0;

prm = xt(1:ng);
por = xt(ng+1:2*ng); 
prf = xt(2*ng+1:3*ng); 
sat = xt(3*ng+1:4*ng); 
for j = 1 : ne
    prme(:,j) = grd(1:ng,j);
    pore(:,j) = grd(ng+1:2*ng,j); 
    prfe(:,j) = grd(2*ng+1:3*ng,j); 
    sate(:,j) = grd(3*ng+1:4*ng,j);
end

% specify the property that is to be plotted
fld = log10(convertTo(prm,milli*darcy)); flde = log10(convertTo(prme,milli*darcy));

% ensemble mean property
mflde = mean(flde,2);

% set invalid or unwanted values to nan
fld(~isfinite(fld)) = nan; flde(~isfinite(flde)) = nan; mflde(~isfinite(mflde)) = nan;
% flde(flde<-10) = nan; mflde(mflde<-10) = nan;

%% plotting
nr = 1; nc = 1; em = [4];

% figure(1)
% subplot(nr,nc,1)
% axis equal
% plotCellData(grid, fld, 'EdgeColor', 'k', 'FaceAlpha', 1.0); hold on
% plotWell(grid, well, 'height', 75, 'color', 'k');
% axis tight off, view(1,38), h=colorbar('horiz'); zoom(1)
% caxis([1 3.5])

% figure(2)
% subplot(nr,nc,1)
% axis equal
% plotCellData(grid, mflde, 'EdgeColor', 'k', 'FaceAlpha', 1.0); hold on
% plotWell(grid, well, 'height', 75, 'color', 'k');
% axis tight off, view(1,38), 
% h=colorbar('horiz'); zoom(1)
% caxis([1 3.5])

for j = 1:length(em)
    subplot(nr,nc,j)
    axis equal
    plotCellData(grid, flde(:,em(j)), 'EdgeColor', 'k', 'FaceAlpha', 1.0);
    plotWell(grid, well, 'height', 75, 'color', 'k');
    axis tight off, view(1,38), 
    h=colorbar('horiz'); zoom(1)
    caxis([1 3.5])
end


%% printing

set(gcf,'PaperUnits','normalized')
set(gcf,'PaperType','a4letter')
set(gcf,'PaperPosition',[.01 .01 .99 .99])
set(gcf,'PaperOrientation','Landscape')
figname=fullfile(pwd,['plotGrid.pdf']);
print(gcf,'-dpdf',figname);


