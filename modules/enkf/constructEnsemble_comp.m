%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Construct true state vector xt and ensemble of states A
%
% PARAMETERS:
%
% RETURNS:
%   xt      - Initial permeability, porosity, pressure and saturation field
%             for truth 
%   A       - Initial permeability, porosity, pressure and saturation field
%             for ensemble
%   nstat   - The number of each kind of state
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

 prm = rock.perm(:,1);
 por = rock.poro;
 sat = states_new.s(:,1);
 prf = states_new.pressure;
 xt = [prm; por; prf; sat]; A = [];
 if nmembers > 0
     for j=1:nmembers
         prme(:,j) = rockE{j}.perm(:,1);
         pore(:,j) = rockE{j}.poro;
         prfe(:,j) = statesE_new{1,j}.pressure;
         sate(:,j) = statesE_new{1,j}.s(:,1);
         % Assert whether there exists fluidE
         if exist('noE','var')
             nwe(:,j) = nwE(j);
             noe(:,j) = noE(j);
             swe(:,j) = SwE(j);
             soe(:,j) = SoE(j);
         end    
     end
     A = [prme; pore; prfe; sate];
      % Assert whether there exists fluidE
     if exist('noE','var')
         A = [prme; pore; prfe; sate; nwe; noe; swe; soe];
     end    
 end 
 nstat = [length(prm),length(por),length(prf),length(sat)];  
  % Assert whether there exists fluidE (The number of each fluid ensemble is 1.) 
  if exist('noE','var')
     nstat = [length(prm),length(por),length(prm),length(por),4];
  end
  
 clear prm* por* prf*

 if ~isempty(G)
     A = [A; G];
     nstat = [nstat, size(G,1)];
 end
 
 if inflation > 0
     B = randn(nb,nmembers);
     for i = 1:nb
         tmp = B(i,:);
         tmp = tmp - mean(tmp); % remove mean
         tmp = tmp ./ std(tmp); % normalize std
         B(i,:) = tmp;
     end
     A = [A; B];
 end
 A0 = A;