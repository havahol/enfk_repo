%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   EnKF update with covariance localization in (x,y) space.
%
% PARAMETERS:
%
% RETURNS:
%   Updated ensemble of model states U
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
nx = grid.cartDims(1);
ny = grid.cartDims(2);
nz = grid.cartDims(3);
ne = nmembers;

U = A;

i0 = 0;
for ix = 1 : nx
if (ix > i0), disp(['ix = ' num2str(ix) ' (' num2str(nx) ')']), i0 = ix; end    
for iy = 1 : ny
    
  % check if there are active cells in the current grid column
  ic = ((1:nz)-1)*nx*ny + (iy-1)*nx + ix;
  [tf, idg] = ismember(ic, imap); idg = idg(idg~=0);
  if ~isempty(idg)
  
  % sorted list if active cell indices in current column
  active = sort(imap(idg)); na = length(active);

  % Find local well data
  lobs = []; dobs = [];
  id = find(loc > 0);
  if ~isempty(id)
      for j = 1 : length(id)
         [xj, yj, zj] = getIndex(loc(id(j)),nx,ny);
         do = sqrt((xj-ix)^2 + (yj-iy)^2);
         if do < 2*range
           lobs = [lobs id(j)];
           dobs = [dobs do];
         end
      end
  end
  
  % Find local (active) grid data
  id = find(loc < 0);
  if ~isempty(id)
      for i = max(1,ix - 2*range + 1) : min(nx,ix + 2*range - 1)
        for j = max(1,iy - 2*range + 1) : min(ny,iy + 2*range - 1)
          do = sqrt((i-ix)^2 + (j-iy)^2);
          if do < 2*range
            ic = ((1:nz)-1)*nx*ny + (j-1)*nx + i;
            [tf, jc] = ismember(ic, imap); 
            jc = jc(jc~=0);
            if ~isempty(jc)
                jd = sort(imap(jc)); 
                [c, ia, iloc] = intersect(jd, abs(loc(id)));
                if ~isempty(iloc)
                    lobs = [lobs iloc];
                    dobs = [dobs do*ones(numel(iloc),1)];
                end
            end
          end
        end
      end
  end

  if ~isempty(lobs)
  
  % find local state vector
  Al = zeros(na*length(nstat),ne);
  for i = 1 : length(nstat)
    var = A(sum(nstat(1:i-1))+1:sum(nstat(1:i)),:);
    Al((i-1)*na+1:i*na,:) = var(idg,:);
  end
  Am = mean(Al,2);
  Al = Al - repmat(Am,1,ne);      
      
  % construct local matrices
  HA = D(lobs,:);
  Sl = (HA-repmat(mean(HA,2),1,ne))/sqrt(ne-1);
  Yl = Y(lobs,:);
  Rl = R(lobs);
  
  % update incorporating covariance localization
  rho = zeros(size(Al,1),length(lobs));
  for i = 1 : length(lobs)
      rho(:,i) = gaspari(dobs(i),range);
  end
  P1 = rho .* (Al * Sl');
  
  P2 = Sl*Sl';
  rho = eye(length(lobs));
  for i = 1 : length(lobs)
    [xi, yi, zi] = getIndex(abs(loc(lobs(i))),nx,ny);
    for j = i + 1 : length(lobs)
      [xj, yj, zj] = getIndex(abs(loc(lobs(j))),nx,ny);
      z = sqrt((xj-xi)^2 + (yj-yi)^2);
      rho(i,j) = gaspari(z,range);
      rho(j,i) = rho(i,j);
    end
  end

  P2 = rho .* P2 + diag(Rl);   
  W = P1 / P2;

  Ul = Al + repmat(Am,1,ne) + beta * sqrt(ne-1) .\ (W * (Yl - HA));

  for i = 1 : length(nstat)
    var = U(sum(nstat(1:i-1))+1:sum(nstat(1:i)),:);
    var(idg,:) = Ul((i-1)*na+1:i*na,:);
    U(sum(nstat(1:i-1))+1:sum(nstat(1:i)),:) = var;
  end

  end
  
  end % column contains active cells
  
end
end

clear lobs dobs Al Am HA Sl Yl Rl P1 P2 Ul W rho z
