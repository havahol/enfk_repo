%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Simulate synthetic truth and model ensemble to the next update time or to the 
%   specified end time and store production time series.
%
% PARAMETERS:
%
% RETURNS:
%   rSol, rSolE         -   MRST states for truth and ensemble
%   wellData, wellDataE -   production time series
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
linsolve_p = @mldivide;  % Pressure
linsolve_t = @mldivide;  % Transport (implicit)


istep = 1;

% Assert whether it restart from time zero and uses the previous simulation time.   
if       t == 0 
   ts = 1;             % run from time zero
elseif   t > 0 
   ts = ts_or;    % run from the last update time  
end

while t < tu

%      step = dt; if t + dt > tu, step = tu-t; end
     step = dt(ts); if t + dt(ts) > tu, step = tu-t; end
     % simulate truth 
     rSol = implicitTransport(rSol, Grid, step, rock, fluid, ...
                     'wells', well, 'LinSolve', linsolve_t);
     % Check for inconsistent saturations
     assert(max(rSol.s(:,1)) < 1+eps && min(rSol.s(:,1)) > -eps);
     % Update solution of pressure equation
     rSol = incompTPFA(rSol, Grid, trans, fluid, 'wells', well, ...
             'LinSolve', linsolve_p);

     % simulate ensemble
     for j = 1 : nmembers
       if ~isempty(G)
          grd = gridE{j};
       else
          grd = Grid;
       end
       if t == 0
           trans = computeTrans(grd, rockE{j});
       end
       rSolE{j} = implicitTransport(rSolE{j}, grd, step, rockE{j}, fluid, ...
                     'wells', wellE{j}, 'LinSolve', linsolve_t);
       % Check for inconsistent saturations
       assert(max(rSolE{j}.s(:,1)) < 1+eps && min(rSolE{j}.s(:,1)) > -eps);
       % Update solution of pressure equation
       rSolE{j} = incompTPFA(rSolE{j}, grd, transE{j}, fluid, 'wells', wellE{j}, ...
                     'LinSolve', linsolve_p);     
     end
     
     t = t + step;  time = convertTo(t,day);
     % Store the index of last time step
     if tu >= cumsum(dt(1:ts))
            ts = ts + 1; 
     end
     if  iter == 1  
            ts_or = ts;
     end
     %---------------------------------------------------------------------------
     % store production data time series (rates in m^3/s, pressures in Pa)
     %---------------------------------------------------------------------------    
     storeProduction;

     if t > 0
        rSol1 = rSol;
     end

     timeSteps = [timeSteps; time]; istep = istep + 1;
     clear rate flow pressure fracflow

end

% clf
% plotCellData(grid, rSolE{1}.s(1:grid.cells.num,1), ...
%         'EdgeColor', 'k', 'EdgeAlpha', 0.1);
% title('Saturation'), colorbar;
% plotWell(grid, wellE{1}, 'height', 50, 'color', 'k');
% axis tight off; view(-80,36); caxis([0 1])
% zoom(1.3)

disp(['finished all simulations. time = ' num2str(time)])    
