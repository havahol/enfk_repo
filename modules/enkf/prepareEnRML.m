%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Initialization, stopping criteria and step size adjustment when iterating with
%   the EnRML.
%
% PARAMETERS:
%
% RETURNS:
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

 if iter == 1
    % initialize iteration
    Y0 = Y;
    J = [];
    beta = 0.5;
    Jmin = 1.e+32;
    if exist(EnkfFile,'file'), delete(EnkfFile); end
 else
    % load initial state and data and iteration history
    load([workdir filesep 'states_' num2str(time) '_' num2str(iter-1) '.mat'],'A0','Y0','A1','J','beta');
    Jmin = min(J);
    % use initial perturbed ensemble of measurements instead? not clear
%   Y = Y0;
%   mse = evaluateMSE(Y, D, R);
 end
 
 if (mse <= nobs) || ((iter > 1) && (mse < Jmin) && (Jmin-mse < epsilon(2)*Jmin))
    % stop if misfit is small enough 
    U = A;
    iter = niter;
 end

 if mse > Jmin
    % reject update and reduce step size
    U = A1; 
    beta = 0.5*beta;
    % stop if step size becomes too small
    if beta < 0.05
       iter = niter;
    end
 else
    % accept update, increase step size and perform update
    J = [J mse];
    U = A;
    A1 = A;
    if iter > 1
        beta = min(1.25*beta, 1);
    end
 end
 