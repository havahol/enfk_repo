%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Initialization of models before forward simulation.
%
% PARAMETERS:
%
% RETURNS:
%   state0, state0E     -   MRST initial states for truth and ensemble
%   model, modelE       -   MRST initial reservoir model
%   dynamic state and structure arrays for the truth and for all ensemble models
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
try
    require ad-props ad-blackoil ad-core
catch %#ok<CTCH>
    mrstModule add ad-props ad-blackoil ad-core
end



% Structural updation
if t == 0 && (iter > 1 || iu > 1)
  if length(nstat) > 5      % 4 is transfered into 5 because we have additional fluid parameters
    load(EnkfFile,'U');
    G = U(sum(nstat(1:4))+1:sum(nstat),:);
    clear U
  end
end

% Set up matlab structures for truth (l=1) and ensemble members (l=2)
for l = 1 : 2

 if l == 1
   ne = 0;
 else
   ne = nmembers;
 end

 for j = min(1,ne) : ne

   % Construct the model
   if j == 0 || ~isempty(G)
     if  exist('Re2Data','var') && Re2Data == true
     [g, wells, fluid, rock, p0, s0, phi, K, G,nwE,noE,SwE,SoE,kwE,koE] = getModel(workdir,modelname,nmembers,j,G);
     else
     [g, wells, fluid, rock, p0, s0, phi, K, G] = getModel(workdir,modelname,nmembers,j,G);      
     end    
     imap = g.cells.indexMap;
     if j == 0
        Grid = g; 
     end
     if j > 0 && ~isempty(G)
        gridE{j};
     end
   end
 
   % assign rock properties
   if j > 0
     if iter == 1 && iu == 1
       rockE{j}.perm = [K(:,j) K(:,j) K(:,j)];   
       rockE{j}.poro = phi(:,j);
           
     else
       load(EnkfFile,'U');
       rockE{j}.perm = [U(1:nstat(1),j) U(1:nstat(1),j) U(1:nstat(1),j)/10]; 
       rockE{j}.poro = U(nstat(1)+1:sum(nstat(1:2)),j);
       rSolE{j}.pressure = U(sum(nstat(1:2))+1:sum(nstat(1:3)),j);
       rSolE{j}.s = U(sum(nstat(1:3))+1:sum(nstat(1:4)),j);
       
       % Add pressure option of compressible fluid
       states{j,1}.pressure = U(sum(nstat(1:2))+1:sum(nstat(1:3)),j);
       states{j,1}.s = U(sum(nstat(1:3))+1:sum(nstat(1:4)),j); 
       
       if exist('nwE','var')
          nwE(j) = U((sum(nstat(1:4))+1),j);
          noE(j) = U((sum(nstat(1:4))+2),j);
          SwE(j) = U((sum(nstat(1:4))+3),j);
          SoE(j) = U((sum(nstat(1:4))+4),j);
          kwE(j) = U((sum(nstat(1:4))+5),j);
          koE(j) = U((sum(nstat(1:4))+6),j);
       end    
       clear U
     end
   end
 
   % Define wells, well control targets and compute grid connections
   W = [];
   g = Grid; if j > 0 && ~isempty(G), g = gridE{j}; end
   r = rock; if j > 0, r = rockE{j}; end
   
   for iw = 1 : length(wells)
     X = wells{iw}{1}; Y = wells{iw}{2};  Z =  1:g.cartDims(3); 
     
     if length(wells{iw})== 8
     Z = wells{iw}{8};
     end
     
     type = 'rate'; val = wells{iw}{3}; 
     if wells{iw}{3}<0, type = 'bhp'; val = wells{iw}{4}; end
     radius = wells{iw}{5}; comp = wells{iw}{6}; name = wells{iw}{7};
     W = verticalWell(W, g, r, X, Y, Z, 'Type', type, ...  
         'Val', val, 'Radius', radius, 'Comp_i', comp, 'name', name);
   end
   if j == 0
     well = W;
   else
     wellE{j} = W;
   end
   
   % TO DO: 
   
   if t == 0
     % Delete corresponding data due to data base simulation
     if j == 0

         grd = Grid;
         % Build grid model
         model = GenericBlackOilModel(grd, rock, fluid);
         model.gas=false;
         model.OutputStateFunctions = {};
         % Initial state
         states0 = initState(Grid, well, p0, [0.2,0.8]);

     else    
         
         grd = Grid; if ~isempty(G), grd = gridE{j}; end

         states0E{j} = initState(grd, wellE{j}, p0, [0.2,0.8]);
         % Assert whether there is fluidE. Change into fluidE coefficient!
         if exist('noE','var')
             fluidE{j} = initSimpleADIFluid('mu' , [   .3,  5]*centi*poise  , ...
                           'rho', [1000, 700]*kilogram/meter^3, ...
                           'n'  , [ nwE(j), noE(j)]                 , ...
                            'smin' , [ SwE(j), SoE(j)]              , ...   
                           'cR',    1e-5/barsa, ...
                           'phases', 'wo');
         else
             fluidE{j} = fluid;
         end   
                       % Add initial model
            modelE{j} = GenericBlackOilModel(grd, rockE{j}, fluidE{j});
            modelE{j}.gas=false;
            modelE{j}.OutputStateFunctions = {};

         
     end
   end

 end

end

clear ne
       