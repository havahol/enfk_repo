function [mse] = evaluateMSE(Y,D,R)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [mse] = evaluateMSE(Y,D,R)
%
% DESCRIPTION:
%   Evaluate the normalized ensemble-mean squared difference between perturbed and
%   simulated data.
%
% PARAMETERS:
%   Y           -   ensemble matrix with perturbed data
%   D           -   ensemble matrix with simulated data
%   R           -   data error variances
%
% RETURNS:
%   mse         -   normalized mean squared error
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

no = size(Y,1);
ne = size(Y,2);

if no > 0 && ne >= 1
   e = D - Y;
   mse = 0;
   for j = 1 : ne
     for i = 1 : no
       mse = mse + 0.5 * e(i,j) * e(i,j) / R(i);
     end
   end
   mse = mse / ne;
else
   mse = -1;
end
