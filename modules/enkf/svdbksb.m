function [X] = svbksb(A,B)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [X] = svbksb(A,B))
%
% DESCRIPTION:
% Solves A*X=B for X by SVD decomposition and back substitution. See
% Numerical Recipes.
%
% PARAMETERS:
%   A           -   normal matrix
%   B           -   right-hand side
%   
%
% RETURNS:
%   Y           -   solution
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------


m=size(A,1);
n=size(A,2);

[u,y,v]=svd(A);         %A=u*w*v'

for j=1:min(m,n), w(j)=y(j,j); end
w(w<1.e-6*max(w))=0;    %threshold
if m<n, w=[w zeros(1,n-m)]; end
    
for k=1:size(B,2)
for j=1:n
    if w(j)~=0
        tmp(j)=(u(:,j)'*B(:,k))/w(j);
    else
        tmp(j)=0;
    end
end
for j=1:n
    x(j)=v(j,:)*tmp';
end
X(:,k)=x;
end