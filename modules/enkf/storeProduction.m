%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Store injection and production results in the structures wellData/wellDataE
%
% PARAMETERS:
%
% RETURNS:
%   wellData, wellDataE -   production time series
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------            

 if ~exist('wellData','var')
     for i = 1 : length(well)
         wellData(i).loc  = Grid.cells.indexMap(well(i,1).cells);
         wellData(i).bhp  = [];
         wellData(i).flx  = [];
         wellData(i).ffl  = [];
         wellData(i).icv  = [];
         wellDataE(i).bhp = [];
         wellDataE(i).flx = [];
         wellDataE(i).ffl = [];
         wellDataE(i).icv = [];
     end
     timeSteps = [];
 end       
 for i = 1 : length(rSol.wellSol)
     wellData(i).bhp = [wellData(i).bhp; rSol.wellSol(i).pressure];
     rate = 0;
     for k=1:length(rSol.wellSol(i).flux)
        wellData(i).icv = [wellData(i).icv; rSol.wellSol(i).flux(k)];
        rate = rate + rSol.wellSol(i).flux(k);
     end
     wellData(i).flx = [wellData(i).flx; rate];

     % compute fractional flow assuming horizontal flow and no 
     % capillary pressure: fw = qw/qt = 1/(1+(muw/krw)*(kro/muo))
     s = mean(rSol.s(well(i).cells));
     kr = fluid.relperm(s); % [krw kro]
     mu = fluid.properties(s); % [muw muo]
     mo = bsxfun(@rdivide, kr, mu); % [krw/muw kro/muo]
     fw = 1/(1 + mo(2)/mo(1));
     if isnan(fw), fw = 0; end
     wellData(i).ffl = [wellData(i).ffl; fw];

     if nmembers > 0
       pressure = zeros(1,nmembers);
       fracflow = zeros(1,nmembers);
       flow = zeros(length(rSol.wellSol(i).flux),nmembers);
       for j = 1 : nmembers
         pressure(1,j) = rSolE{j}.wellSol(i).pressure;
         s = mean(rSolE{j}.s(well(i).cells));
         kr = fluid.relperm(s);
         mu = fluid.properties(s);
         mo = bsxfun(@rdivide, kr, mu);
         fw = 1/(1 + mo(2)/mo(1));
         if isnan(fw), fw = 0; end
         fracflow(1,j) = fw;
       end
       wellDataE(i).bhp = [wellDataE(i).bhp; pressure];
       for k = 1 : length(rSol.wellSol(i).flux)
         for j = 1 : nmembers
            flow(k,j) = rSolE{j}.wellSol(i).flux(k);
         end
         wellDataE(i).icv = [wellDataE(i).icv; flow(k,:)];
       end
       wellDataE(i).flx = [wellDataE(i).flx; sum(flow,1)];
       wellDataE(i).ffl = [wellDataE(i).ffl; fracflow];
     end
 end