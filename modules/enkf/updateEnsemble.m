%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Update the ensemble with method of choice 
%
% PARAMETERS:
%
% RETURNS:
%   Updated ensemble of model states U
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST.

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 


if strcmp(method,'EnRML')

   U = EnRML(D, Y, R, A, A0, beta);

elseif strcmp(method,'EnKF')

   U = EnKF(D, Y, A, scheme, R, tolerance, beta);
      
elseif strcmp(method,'LCEnKF')

   LCEnKF;
   
else  

   error(' *** method not yet implemented ***');

end 
