function [G,wells,fluid,rock,p0,s0,phi,K,S,nwE,noE,SwE,SoE,kwE,koE] = makeModel(modelname,ne,j,S)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [G,wells,fluid,rock,p0,s0,phi,K,S] = makeModel(modelname,ne,j,S)
%
% DESCRIPTION:
%   Obtain a reservoir model description including grid structure, fluid
%   and rock properties, well descriptions, and the initial state, as well
%   as ensemble of permeability, porosity and structural variables (if
%   structural updating is desired).
%
% PARAMETERS:
%   modelname   -   name of the model (there has to be a .m file with the
%                   same name)
%   ne, j       -   ensemble size and current member
%   S           -   structural parameter array
%
% RETURNS:
%   G           -   structural grid
%   wells       -   description of well in the model
%   fluid       -   fluid properties
%   rock        -   permeability and porosity of the truth model
%   p0, s0      -   initial pressure and saturation
%   phi, K, S   -   ensembles of porosity, permeability and structure
%
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
if ~exist([modelname '.m'],'file')
    
      error('*** no valid model specified ***');
      
else
    
      eval(modelname)
      
end
