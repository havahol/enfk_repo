%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Input file of the Ensemble Kalman Filter module for the SINTEF reservoir 
%   simulator MRST.
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

%----------------------------------------------------------------------------------
% General settings
%----------------------------------------------------------------------------------
 modelname      = 'testGrid3_incomp';
 
%---------------------------------------------------------------------------------- 
% EnKF settings
%----------------------------------------------------------------------------------
 method         = 'EnKF';           % method (EnKF,LCEnKF,EnRML). For ES-MDA, use EnKF with niterations > 1
 scheme         = 1;                % update scheme (when method is EnKF)
                                    % 1: inverse computed using Matlab functions \ and / and full rank R
                                    % 2: pseudo inverse with full rank R
                                    % 3: subspace pseudo inverse with low rank Re (default)
 nmembers       = 2;               % ensemble size
 restart        = 1;                % 0: continue simulation after each update
                                    % 1: restart from time 0 after each update
 inflation      = 0;                % 0: no inflation, 1: adaptive, other: user specified
 transform      = {1 0 ...          % transformation functions for the states
                   0 [2 0.2 0.8]};  % see applyTransform.m for details
 niterations    = 1;                % max number of iterations to same update time
                                    % 0: no update, 1: one update, 2: use same data twice, etc.
 Alpha          = [1];              % data error-variance inflation factors for niterations
                                    % e.g. for 4 ES-MDA iterations: alpha = [9.333 7 4 2];
 nrepeats       = 0;                % rumber of repeats of the experiment
 tolerance      = 0.01;             % tolerance used in pseudo inversion 
 isIncomp       = 1;                % assert whether inputSetting belongs to incompressible or compressible flow
%----------------------------------------------------------------------------------
% Observations
%----------------------------------------------------------------------------------
 obstype        = {'flx' 'bhp' 'wct'};  % observation type (flx, bhp, wct, sat)
 transformobs   = {0 0 0};              % transformation parameters for observations  
 Sigma          = [50 * meter^3/day,... % observation error standard deviations
                    5 * barsa(),...     % for each observation type
                    0.1];
 range          = 5;                    % localization range: c(2*range) = 0, where 
                                        % for c the Gaspari-Cohn function is used. only
                                        % used with LCEnKF
                                        
 oneobs         = [];                   % index of single well or grid cell observation
                                        % not used if empty (for testing purposes only)
 
%----------------------------------------------------------------------------------
% Time steps
%----------------------------------------------------------------------------------
 tsim           =  45.00*year();    % simulation end time
 tend           =  30.00*year();    % history match end time
 dt             =   0.50*year();    % simulation time step
 dtu            =   5.00*year();    % update time interval
 dto            =   1.00*year();    % well data time interval (multiple of dt)                
 dts            =  10.00*year();    % seismic data time interval (multiple of dtu)
                                    % only used when obstype includes 'sat' 
 
%----------------------------------------------------------------------------------
% End of input
%----------------------------------------------------------------------------------

 tda = union(dtu:dtu:tend,tend);
 tdo = union(dto:dto:tend,tend);
 tds = union(dts:dts:tend,tend);
 tda = union(tda,tsim);

 if isempty(intersect(obstype,{'flx' 'bhp' 'wct'}))
     dto = dts;
 end

 itransform = transform;
 for i = 1 : length(transform)
     itransform{i}(1) = -1 * transform{i}(1);
 end

 obsnum = zeros(1,numel(obstype)); % 1: flx,2: bhp,3: wct,4: sat, 5: dst
 for i = 1:numel(obstype)
     obsnum(i) = find(strncmp(obstype{i},{'flx' 'bhp' 'wct' 'sat' 'dst'},3),1);
 end
 
 % initialisation of geo-model/structural parameters
 G = []; 
 
 Settings = struct(...
     'workdir',     workdir,    ...
     'modelname',   modelname,  ...
     'method',      method,     ...
     'scheme',      scheme,     ...
     'nmembers',    nmembers,   ...
     'restart',     restart,    ...
     'inflation',   inflation,  ...
     'transform',  {transform}, ...
     'niterations', niterations,...
     'alpha',       Alpha,      ...
     'nrepeats',    nrepeats,   ...
     'tolerance',   tolerance,  ...
     'tsim',        tsim,       ...
     'tend',        tend,       ...
     'dt',          dt,         ...
     'dto',         dto,        ...
     'dts',         dts,        ...
     'dtu',         dtu,        ...
     'transformobs',{transformobs},...
     'obsnum',      obsnum,     ...
     'sigma',       Sigma,      ...
     'isIncomp',    isIncomp);
 
 if niterations == 0
     restart = 0;
 end
 
 % rows in dummy matrix B for adaptive inflation
 nb = 100;
 
 % iteration and step size parameters
 J = [];
 Jmin = 1.e+32;
 beta = 1.0;
 epsilon = [1.e-6 1.e-6];
  