function [Grid,wells,fluid,rock,p0,s0,phi,K,G,varargout] = getModel(workdir,modelname,ne,j,G)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [grid,wells,fluid,rock,p0,s0,phi,K,G] = getModel(workdir,modelname,ne,j,G)
%
% DESCRIPTION:
%   Obtain a reservoir model description including grid structure, fluid
%   and rock properties, well descriptions, and the initial state, as well
%   as ensemble of permeability, porosity and structural variables (if
%   structural updating is desired).
%
% PARAMETERS:
%   workdir     -   working folder
%   modelname   -   name of the model (there has to be a .m file with the
%                   same name)
%   ne, j       -   ensemble size and current member
%   G           -   structural parameter array
%
% RETURNS:
%   grid        -   structural grid
%   wells       -   description of wells in the model
%   fluid       -   fluid properties
%   rock        -   permeability and porosity of the truth model
%   p0, s0      -   initial pressure and saturation
%   phi, K, G   -   ensembles of porosity, permeability and structure
%   nwE, noE, SwE, SoE, kwE, koE   -   ensembles of corey function
%   parameters
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

if exist([workdir modelname '.mat'],'file') && isempty(G)
    if nargout > 9
        load([workdir filesep modelname  '.mat'],'Grid','wells','fluid','rock','p0','s0','phi','K','G','nwE','noE','SwE','SoE','kwE','koE');
        varargout{1}=nwE;
        varargout{2}=noE;
        varargout{3}=SwE;
        varargout{4}=SoE;
        varargout{5}=kwE;
        varargout{6}=koE;
    elseif nargout <= 9
        load([workdir filesep modelname  '.mat'],'Grid','wells','fluid','rock','p0','s0','phi','K');
    end
else
%     [grid,wells,fluid,rock,p0,s0,phi,K,G] = makeModel(modelname,ne,j,G);
%     save([modelname '.mat'],'grid','wells','fluid','rock','p0','s0','phi','K','G');
    % Consider the fluidE effects
    if nargout > 9
        [Grid,wells,fluid,rock,p0,s0,phi,K,G,nwE,noE,SwE,SoE,kwE,koE] = makeModel(modelname,ne,j,G);
        save([workdir filesep modelname  '.mat'],'Grid','wells','fluid','rock','p0','s0','phi','K','G','nwE','noE','SwE','SoE','kwE','koE');    
        varargout{1}=nwE;
        varargout{2}=noE;
        varargout{3}=SwE;
        varargout{4}=SoE;
        varargout{5}=kwE;
        varargout{6}=koE;
    elseif nargout <= 9
            [Grid,wells,fluid,rock,p0,s0,phi,K,G] = makeModel(modelname,ne,j,G);
            save([workdir filesep modelname '.mat'],'Grid','wells','fluid','rock','p0','s0','phi','K','G');
    end
    
end



return