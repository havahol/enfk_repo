
% workDir = 'E:\Olwijn\MRST-2011a\TESTING1d_update1_2.0\'; 
workDir = [ROOTDIR 'ENKF/']; %'/Users/olwijn/Werk/Matlab/mrst-2013a/TEST/'
time    = 1825; 
nx      = 250;
ny      = 1;
nz      = 1;
it      = 1;
ptype   = 1;

%% states

load([workDir 'states_' num2str(time) '_' num2str(it) '.mat']);

ne = size(U,2);
ng = nx * ny * nz;
dx = 1000/nx; dy = 25/ny; dz = 25/nz;
x  = [dx/2:dx:(nx-0.5)*dx];

prm = xt(1:ng); 
por = xt(ng+1:2*ng); 
prf = xt(2*ng+1:3*ng); 
sat = xt(3*ng+1:4*ng);
for j = 1 : ne
    prme(:,j) = U(1:ng,j); 
    pore(:,j) = U(ng+1:2*ng,j); 
    prfe(:,j) = U(2*ng+1:3*ng,j); 
    sate(:,j) = U(3*ng+1:4*ng,j);
end

if ptype == 1

figure(1)
subplot(3,1,1)
ln = '-'; lw = 1.25; clr = [0.5 0.5 0.5]; mk = 'none'; ms = 2;
for j = 1 : 1 : ne
    fld = sate(:,j); 
%     fld = log(convertTo(prme(:,j),milli*darcy)); 
    h = plot(x,fld,'-g'); hold on
    set(h,'Linestyle',ln,'LineWidth',lw,'Color',clr,'Marker',mk,'MarkerSize',ms,'MarkerFaceColor',clr,'MarkerEdgeColor',clr);
end
ln = '--'; lw = 2; clr = 'k'; mk = 'none'; ms = 2;
fld = sat; 
% fld = log(convertTo(mean(prme(:,:),2),milli*darcy));  
h = plot(x,fld,':k');
set(h,'Linestyle',ln,'LineWidth',lw,'Color',clr,'Marker',mk,'MarkerSize',ms,'MarkerFaceColor',clr,'MarkerEdgeColor',clr);

ln = '-'; lw = 2; clr = 'k'; mk = 'none'; ms = 2;
fld = sat; 
% fld = log(convertTo(prm,milli*darcy));  
h = plot(x,fld,'-k');
set(h,'Linestyle',ln,'LineWidth',lw,'Color',clr,'Marker',mk,'MarkerSize',ms,'MarkerFaceColor',clr,'MarkerEdgeColor',clr);

set(gca,'Linewidth',1.5,'Fontsize',14,'Ylim',[0.2 0.8]); 
xlabel('x (m)','Fontsize',16)
ylabel('saturation','Fontsize',16) %'log permeability'

h=text(0,0.9,'(b)'); %h=text(5,8.8,'(d)');
set(h,'FontSize',22)

end

%% observations

if ptype ==2

% load([workDir 'gtt_t' num2str(time) '_i' num2str(it) '.mat']); flde = gtte; fld = gtt;
load([workDir 'dst_t' num2str(time) '_i' num2str(it) '.mat']); flde = dste; fld = dst;

% fld(fld==2774) = [];

figure(2)
for k = 1 : numel(fld)

    subplot(2,2,k)
    xmin = 6; xmax = 7;
%   fld = convertTo(flde,milli*darcy);
%   [n1,out1] = hist(reshape(log(fld),numel(fld),1),11);
    [n1,out1] = hist((flde(k,:)),11); %[xmin:(xmax-xmin)/21:xmax]);
    K = 0.5;
    bar1 = bar(out1, n1, 'FaceColor', [0.5 0.5 0.5], 'EdgeColor', 'k');
%   set(bar1,'BarWidth',K);
    hold on;

    [~,out2] = hist((fld(k)),1); % log(fld)
%   id = find(abs(out1-out2)==min(abs(out1-out2)));
%   n2 = nan*n1; out2 = out1; 
    n2 = 15; %n2(id) = 15; %0.75*n1(id);
    bar2 = bar(out2, n2, 'FaceColor', 'k', 'EdgeColor', 'k');
    set(bar2,'BarWidth',K/1);
    
%   hold off; 
%   legend('series1','series2')
    aw = 2; fs = 18; 
    set(gca,'Linewidth',aw,'Fontsize',fs);
%   set(gca,'XLim',[6 7]); %,'YLim',[0 100]);
    set(gca,'Linewidth',1.5,'Fontsize',12);
    xlabel('log(time)','Fontsize',14)

end

% h=text(6.01,108,'(b)');
% set(h,'FontSize',18)

end


%% test

% load([workDir 'test_' num2str(time) '_iter' num2str(it) '.mat']);
% 
% ix = 40;
% sat = sat_test(ix,:);
% x = t_test;

% figure(3)
% subplot(3,1,1)
% h = plot(x,sat,'-k');
% ln = '-'; lw = 2; clr = 'k'; mk = 'none'; ms = 2;
% set(h,'Linestyle',ln,'LineWidth',lw,'Color',clr,'Marker',mk,'MarkerSize',ms,'MarkerFaceColor',clr,'MarkerEdgeColor',clr);
% set(gca,'Linewidth',1.5,'Fontsize',12);
% set(gca,'YLim',[0.2 0.7]); %'XLim',[130 180],
% xlabel('time (days)','Fontsize',14)
% ylabel('water saturation','Fontsize',14)

% h=text(50,0.775,'(b)');
% set(h,'FontSize',18)

%% save plots

set(gcf,'PaperUnits','normalized')
set(gcf,'PaperType','a4letter')
set(gcf,'PaperPosition',[.01 .01 .99 .99])
set(gcf,'PaperOrientation','Landscape')
figname=fullfile(pwd,['plot1D.pdf']); %' num2str(sp) '
print(gcf,'-dpdf',figname);

figname=fullfile(pwd,['plot1D.eps']); %' num2str(sp) '
print(gcf,'-depsc2',figname);

% eps4ai(figname,'bw')    
