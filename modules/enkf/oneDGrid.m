
gauss = randn(10000); unifm = rand(10000); ir = 9000;

%% Dimensions
    dims(1)=250; dims(2)=1; dims(3)=1; 
    physDims(1)=1000; physDims(2)=25; physDims(3)=25; 

    %% Build grid
    grdecl.cartDims = reshape(dims, 1, []);
    [X, Y, Z]  = ndgrid(linspace(0, 1, dims(1) + 1), ...
                        linspace(0, 1, dims(2) + 1), ...
                        linspace(0, 1, dims(3) + 1));

    X = X .* physDims(1);
    Y = Y .* physDims(2);
    Z = Z .* physDims(3);

    %% Make pilars
    lines          = zeros([prod(dims([1, 2]) + 1), 6]);
    lines(:,[1,4]) = reshape(X(:,:,[1,end]), [], 2);
    lines(:,[2,5]) = reshape(Y(:,:,[1,end]), [], 2);
    lines(:,[3,6]) = reshape(Z(:,:,[1,end]), [], 2);

    grdecl.COORD = reshape(lines', [], 1);

    %% Assign z-coordinates
    % ind(d) == [1, 2, 2, 3, 3, ..., dims(d), dims(d), dims(d)+1]
    ind = @(d) 1 + fix((1 : 2*dims(d)) ./ 2);
    z   = Z(ind(1), ind(2), ind(3));
    
    grdecl.ZCORN = z(:);

    %% Assign active cells
    actnum = ones(dims);
    grdecl.ACTNUM = int32(actnum(:));

    %% Process grid
    G = processGRDECL(grdecl);
    G = computeGeometry(G(1)); 
    clear grdecl

    %% Rock properties
    
%     c = round(200 * dims(1) / physDims(1)); clear w iw
%     nw = 2 * c + 1; iw = 1:nw; iw = iw - mean(iw);
%     for i = 1 : nw, w(i) = gaspari(abs(iw(i)),c/2); end
%     
    for i=1:max(1,ne+1)

%         p1 = randn(dims(1),1);
%         p = convn(p1,w'/sum(w),'same');
%         p = p / std(p);      
% %         K = 100 * exp(p); % opt 1
%         K = 400 + 150 * p; % opt 2
%         K(K < 5) = 5;
     
        % permeability model
        Kave = 0.5*ones(dims(3),1); % Kave = rand([dims(3),1]);
        K = logNormLayers(G.cartDims, Kave, 'sigma', 2); % lognormal values with mean log(Kave)
        K = K(G.cells.indexMap);
        Kmin=min(min(K)); Kmax=max(max(K)); %Kmin=0; Kmax=7;
        K = (100 + (K-Kmin)/(Kmax-Kmin)*500);
        K=log(K); 
        Kmin=min(min(K)); Kmax=max(max(K)); Kmean=mean(mean(K));
        Kave=Kmean*ones(dims(3),1)+(Kmax-Kmean)*randn(dims(3),1)./2;
        K=K-Kmean;
        K = reshape(K,dims(1),dims(3));
        for j=1:dims(3)
            K(:,j)=K(:,j)+Kave(j);
        end
        
%         % test
%         K(:,1:dims(3))=log(2); 
%         K(:,1:4)=log(1800); 
%         K(:,8:11)=log(1800);
        
        K = reshape(K,dims(1)*dims(3),1);
        K=exp(K);
        
        Kall(:,i) = K;

        % porosity model
        phi(:,i) = 0.25 * (K ./200).^0.1; %0.3* ones*size(K); %

        clear K
    end
    
    % load ('E:\Olwijn\MRST-2011a\realizations_1D.mat','Kall','phi');
    
    K = convertFrom(Kall, milli*darcy); clear Kall

    perm = [K(:,end) K(:,end) K(:,end)/10];
    rock.perm = perm; %convertFrom(perm, milli*darcy);
    rock.poro = phi(:,end); %0.25 * (K(:,end) ./200).^0.1; % 0.2315*ones(size(K)); %clear K;

    %% Fluid properties
    fluid = initCoreyFluid('mu' , [   0.4,  0.9]*centi*poise, ...
                         'rho', [1014, 859]*kilogram/meter^3, ...
                         'n'  , [   4,   4]                 , ...
                         'sr' , [ 0.2, 0.2]                 , ...
                         'kwm', [   1,   1]);

    %% Initial state
    p0 = 300*barsa();
    s0 = 0.2;
    bhp = 300*barsa();
    q = 30*meter^3/day;

    %% Well definitions and constraints
    % Set vertical injectors
    I = [125]; % 1
    J = [1];
    R = q;
    bhp = 300*barsa(); P = bhp;
    nIW = 1:numel(I); W = [];
    for i = 1 : numel(I),
       W = verticalWell(W, G, rock, I(i), J(i), 1:dims(3), 'Type', 'rate', ...
                        'Val', R(i), 'Radius', 0.1, 'Comp_i', [1,0,0], ...
                        'name', ['I$_{', int2str(i), '}$']);
%        W = verticalWell(W, G, rock, I(i), J(i), 1:dims(3), 'Type', 'bhp', ...
%                         'Val', P(i), 'Radius', 0.1, 'Comp_i', [1,0,0], ...
%                         'name', ['I$_{', int2str(i), '}$']);                
    end

    % Set vertical producers, completed in the upper 14 layers
    I = [1 dims(1)];
    J = [1 1];
    bhp = [275 275]*barsa(); P = bhp; % 250
    nPW = (1:numel(I))+max(nIW);
    for i = 1 : numel(I),
       W = verticalWell(W, G, rock, I(i), J(i), 1:dims(3), 'Type', 'bhp', ...
                        'Val', P(i), 'Radius', 0.1, ...
                        'name', ['P$_{', int2str(i), '}$']);
    end
    

% prm = convertTo(rock.perm(:,1), milli*darcy); % prm = log10(rock.perm(:,1))
% figure(1)
% clf
% axis equal
% plotCellData(G,prm,'EdgeColor','k','FaceAlpha', 0.5);
% %     plotFaces(G, find(mface < 1), log10(mface(mface < 1)));
% axis off, view(15,60), h=colorbar('horiz');
% % cs = [0:100:700]; %[200 400 700 1000 1500 2000];
% % caxis([min(cs) max(cs)]); %caxis(log10([min(cs) max(cs)]*milli*darcy));
% % set(h, 'XTick', cs, 'XTickLabel', num2str(round(cs)')); %set(h, 'XTick', log10(cs*milli*darcy), 'XTickLabel', num2str(round(cs)'));
% zoom(2.5), title('Log_{10} of x-permeability [mD]');
% 
% por = rock.poro;
% figure(2)
% clf
% axis equal
% plotCellData(G,por,'EdgeColor','k','FaceAlpha', 0.5);
% %     plotFaces(G, find(mface < 1), log10(mface(mface < 1)));
% axis off, view(15,60), h=colorbar('horiz');
% cs = [0:0.05:0.4]; %[200 400 700 1000 1500 2000];
% caxis([min(cs) max(cs)]); %caxis(log10([min(cs) max(cs)]*milli*darcy));
% set(h, 'XTick', cs, 'XTickLabel', num2str(cs')); %set(h, 'XTick', log10(cs*milli*darcy), 'XTickLabel', num2str(round(cs)'));
% zoom(2.5), title('poro');
%         