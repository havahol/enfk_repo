%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Store injection and production results in the structures wellData/wellDataE
%
% PARAMETERS:
%
% RETURNS:
%   wellData, wellDataE -   production time series
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------            
for f = 1:length(states)
 if ~exist('wellData','var')
     for i = 1 : length(well)
         wellData(i).loc  = Grid.cells.indexMap(well(i,1).cells);
         wellData(i).bhp  = [];
         wellData(i).flx  = [];
         wellData(i).ffl  = [];
         wellData(i).icv  = [];
         wellDataE(i).bhp = [];
         wellDataE(i).flx = [];
         wellDataE(i).ffl = [];
         wellDataE(i).icv = [];
     end
 end 
     % Store Production data for truth
 for i = 1 : length(states_new.wellSol)
     wellData(i).bhp = [wellData(i).bhp; wellSols1{f,1}(i).bhp];
     % Store inflow control values and well flux
     rate = 0;
     for k=1:size(states_new.wellSol(i).flux)
        wellData(i).icv = [wellData(i).icv; sum(wellSols1{f,1}(i).flux(k,:))];
        rate = rate + sum(abs(wellSols1{f,1}(i).flux(k,:))); 
     end
     wellData(i).flx = [wellData(i).flx; rate];
     % Store the fraction of water
     fw = wellSols1{f,1}(i).qWs/(wellSols1{f,1}(i).qWs+wellSols1{f,1}(i).qOs);
     if isnan(fw), fw = 0; end
     wellData(i).ffl = [wellData(i).ffl; fw];
     
     % Store Production data for ensemble
     if nmembers > 0
       pressure = zeros(1,nmembers);
       fracflow = zeros(1,nmembers);
       flow = zeros(length(states_new.wellSol(i).flux),nmembers);
       % Store inflow well bhp, well flux and control values for ensemble
       for j = 1 : nmembers
         pressure(1,j) = wellSols1E{1,j}{f,1}(i).bhp;
         fw = wellSols1E{1,j}{f,1}(i).qWs/(wellSols1E{1,j}{f,1}(i).qWs+wellSols1E{1,j}{f,1}(i).qOs);  
         if isnan(fw), fw = 0; end
         fracflow(1,j) = fw;
       end
       wellDataE(i).bhp = [wellDataE(i).bhp; pressure];
       for k = 1 : size(wellSols1E{1,j}{f,1}(i).flux)   
         for j = 1 : nmembers
            flow(k,j) = sum(abs(wellSols1E{1,j}{f,1}(i).flux(k,:)));   
         end
         wellDataE(i).icv = [wellDataE(i).icv; flow(k,:)];
       end
       wellDataE(i).flx = [wellDataE(i).flx; sum(flow,1)];
       wellDataE(i).ffl = [wellDataE(i).ffl; fracflow];
     end
 end
end