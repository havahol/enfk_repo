           function U = EnRML(D, Y, R, A, A0, beta)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   function U = EnRML(D, Y, R, A, A0, beta)
%
% DESCRIPTION:
%   EnRML update step
%
% PARAMETERS:
%   Y           -   ensemble of perturbed observations
%   R           -   array with measurement error variances
%   D           -   ensemble of simulated observations
%   A           -   ensemble of simulated states at current iteration
%   A0          -   ensemble of simulated states at iteration 1
%   beta        -   step size
%
% RETURNS:
%   Updated ensemble of model states U
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------            
           nmembers = size(A,2);

           % EnRML - currently only diagonal R
           S = D - repmat(mean(D,2), 1, nmembers);                
           M = A - repmat(mean(A,2), 1, nmembers);
           G = svdbksb(M', S');
           clear M        
           M = (A0 - repmat(mean(A0,2), 1, nmembers)) / sqrt(nmembers-1);
           GM = G' * M;
           if size(GM,1) <= size(GM,2)
             W2 = (GM*GM' + diag(R));
             W  = GM' / W2;
             clear W2
           else
             W = (eye(nmembers) + (GM' / diag(R)) * GM)  \ GM' / diag(R);
           end
           innovation = D - Y - G' * (A - A0);
           MW = M * W;
           % global update
           U = A0 - MW * innovation;
           if beta ~= 1
             U = beta * U + (1-beta) * A; 
           end
           clear W G GM 
