%% Real field model example
%
%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
% This is an experimental example which represents the test grid model.

%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
%% model definition

% grid structure
realization = 1;
[G, rock, fluid, deck] = setupEGG('realization', realization);
 
%% Set rock and fluid data
rock_real = rock;
poro_real  = rock.poro;
perm_real =  rock.perm;

% Load rock properties from .mat file.
 
% rock_ensemble = zeros(size(rock.perm));
% realization_en = 1:100;
% for i = 1 :100
%    [G_en, rock_en, fluid_en, deck_en] = setupEGG('realization', realization_en(i));
%     rock_ensemble_perm{i} = rock_en.perm;
%     rock_ensemble_poro{i} = rock_en.poro;
% end
% 
% rock_Egg = ['rock_Egg.mat'];
% save([ROOTDIR '\examples\datasets\egg\rock_Egg.mat'], 'rock_ensemble_perm','rock_ensemble_poro' );

% Load rock properties from .mat file.
load([ROOTDIR 'examples' filesep 'datasets' filesep 'egg' filesep 'rock_Egg.mat']); 

% Optional location for enkf/examples/datasets/rock_Egg.mat
% load([ROOTDIR 'modules' filesep 'enkf' filesep 'examples' filesep 'datasets' filesep 'rock_Egg.mat']); 

perm = rock_ensemble_perm;
poro = rock_ensemble_poro;
perm{1,ne+1} = perm_real;
poro{1,ne+1} = poro_real;

% Add reasonable rock properties
K = zeros(size((perm_real),1),100);
for i = 1:100
K(:,i) =   perm{1,i}(:,1); 
end
clear perm

phi = zeros(size((perm_real),1),100);
for j = 1:100
phi(:,j) = poro{1,j}(:,1); 
end
clear poro

rock.perm = K(:,end);
rock.poro = phi(:,end);

% Fluid properties
% Generate random variables for fluidE (ne)
nwE = 2*ones(1,ne);
noE = 2*ones(1,ne);
SwE = 0.2*ones(1,ne);
SoE = 0.2*ones(1,ne);
kwE =1*ones(1,ne);
koE =1*ones(1,ne);


% Two-phase template model
    fluid = initSimpleADIFluid('mu' , [   1,  5]*centi*poise  , ...
                           'rho', [1000, 700]*kilogram/meter^3, ...
                           'n'  , [   2,   2]                 , ...
                           'smin' , [ 0.2, 0.2]                 , ...
                           'cR',    1e-5/barsa, ...
                           'phases', 'wo');
    fluidE = cell(ne,1);

%% Introduce wells

% Set vertical injectors
I = [5, 30, 2, 27, 50, 8, 32, 57];
J = [57, 53, 35, 29, 35, 9, 2, 6];
ZI = [1:7; 1:7;1:7; 1:7;1:7; 1:7; 1:7;1:7];
bhp = 1000*barsa(); q = 420*meter^3/day; %150*meter^3/day;
radius = 0.1;
comp = [1,0];
for k = 1 : numel(I)
    name = ['I' int2str(k)];
    wells{k} = {I(k),J(k),q,bhp,radius,comp,name,ZI(k,:)};
end

% Set vertical producers
nW = length(wells);
I = [16, 35, 23, 43];
J = [43, 40, 16, 18];
ZP = [1:7; 1:7;1:7; 1:7];
bhp = 395*barsa(); q = -1;
radius = 0.1;
comp = [0,1];
for k = 1 : numel(I)
    name = ['P' int2str(k)];
    wells{nW+k} = {I(k),J(k),q,bhp,radius,comp,name,ZP(k,:)};
end

%% Initial state

p0 = 400*barsa(); %400*barsa();
s0 = 0.2;