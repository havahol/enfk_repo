       function [Y, R] = perturbObservations(obs, sig, nmembers)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [Y, R] = perturbObservations(obs, sig, nmembers)
%
% DESCRIPTION:
%   Add an ensemble of perturbations to the observations based on provided 
%   measurement error standard deviations.
%
% PARAMETERS:
%   obs             -   measurements
%   sig             -   measurement error standard deviations 
%   nmembers        -   ensemble size
%
% RETURNS:
%   Y           -   ensemble matrix of perturbed measurement vectors
%   R           -   array with measurement error variances
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
       % observations - currently only diagonal R
       y = obs;
       
       nobs = length(y);
           
       noise = randn(max(10000,nobs),1);
        
       for i = 1 : length(y)
           y(i) = y(i) + sig(i)*noise(end-nobs+i);
       end
       R = sig.^2;
       clear sig noise

       % ensemble of perturbed observations
       if nmembers > 0
           Y = repmat(y, 1, nmembers);
           for i = 1:size(Y,1)
             rndm(i,:) = randn(1,nmembers); 
             rndm(i,:) = rndm(i,:) - mean(rndm(i,:)); 
             rndm(i,:) = rndm(i,:) / std(rndm(i,:));
             Y(i,:) = Y(i,:) + sqrt(R(i)) * rndm(i,:);
           end
           clear rndm
       else
           Y = [];
       end
