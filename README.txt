DISCLAIMER
----------

(c) 2008-2017 TNO, The Hague, the Netherlands. All copyrights and other intellectual property rights reserved.

The Matlab EnKF module for MRST is provided for non-commercial purposes only on a strict 'as is' basis. 
TNO does not assume any responsibility or liability for any damage that might result from its use.

The module has been tested with MRST version 2017a and Matlab version R2014b.


CONDITIONS FOR USE
------------------

We expect you to acknowledge TNO for making this data set available in any publications, presentations or 
reports of for results obtained with (parts of) the data set.


REFERENCES
----------

Leeuwenburgh, O., E. Peters, and F. Wilschut, Towards an integrated workflow for structural reservoir model 
updating and history matching, paper SPE 143576 presented ath the SPE EUROPEC/EAGE Annual COnference and 
Exhibition held in Vienna, Austria, 23-26 May 2011.

Leeuwenburgh, O., and R. Arts, Distance parameterization for efficient seismic history matching with the 
Ensemble Kalman Filter, Computational Geosciences, DOI 10.1007/s10596-014-9434-y, 2014.


INSRUCTIONS FOR USE 
-------------------

The EnKF package contains of a set of files organized in a file structure consisting of module folder called 
enkf and 4 example datasets, called 5spot, norne, SPE10 and egg:

> module/startup_enkf.m
> modules/enkf
> examples/datasets/5spot ( for compressible see inputsettings1.m for model TestGrid1, for incompressible see inputsettings1_incomp.m )
> examples/datasets/norne ( for compressible see inputsettings3.m for model TestGrid3, for incompressible see inputsettings1_incomp.m)
> examples/datasets/egg   ( ONLY for compressible see inputsettings_Egg_model.m for model TestGridEgg)
> examples/datasets/SPE10 ( ONLY for compressible see inputsettings_SPE10.m for model TestGrid_SPE10)

This folder structure should be merged into the MRST distribution which can be downloaded from:
http://www.sintef.no/projectweb/mrst/download/

To add the enkf module to the Matlab path, after running the MRST script startup.m, run 
> startup_enkf

EXAMPLE for INCOMPRESSIBLE and COMPRESSIBLE
----------------------

INCOMPRESSIBLE EXAMPLE
---------------------- 
To run the 5spot example for incompressible flow, based on TestGrid1, unzip the contents of 5spot.zip to the folder:
<path to mrst>/examples/datasets/5spot/

To run the example call mrstEnKF_new with the path to the input settings script, e.g.:

> workdir =  mrstEnKF("inputSettings1_incomp.m")

COMPRESSIBLE EXAMPLE
---------------------- 
To run the 5spot example for compressible flow, based on TestGrid1, unzip the contents of 5spot.zip to the folder:
<path to mrst>/examples/datasets/5spot/

To run the example call mrstEnKF with the path to the input settings script, e.g.:

> workdir =  mrstEnKF("inputSettings1.m")

PLOT EXAMPLE
This will return the directory workdir where all the results are saved.
To plot the results call plotProduction() with the path to the results directory, the simulation time at which output is desired, the EnKF iteration number and the subfig pattern required, e.g.: 

> plotProduction(workdir, 10950, 1, [2,3])

PLOTTING 
--------

To plot results, use the scripts plotProduction.m (for plotting time series of well data) 
plot1D.m and plot3D.m (for plotting 1D and 3D views of some reservoir grid property).

If users want to plot reservoir grid property flexible, use the scrpts plot1D_new.m
 and plot3D_new.m, and see the description included in them (for plotting 1D and 3D views of some reservoir grid property).

ES-MDA
------

To use the ES-MDA scheme, choose EnKF as method and set niterations to a value greater than 1, and optionally 
specify alpha values for each iteration. Normally in ES-MDA applications dtu is set equal to tend.


NOTE 
----

In order to run Egg and SPE10 model, users should copy mrst/examples/datasets/egg 
and mrst/examples/datasets/SPE10 and paste them into mrst/examples/datasets.

In order to run the 5spot example(which is included in inputsettings1), first unzip the 5spot folder in examples/datasets. 

In order to run the Norne example(which is included in inputsettings3), first run the script showNorne.m in examples/datasets. This will 
download the Norne data set from GitHub and place it in examples/data.

In order to run the Egg example, first type getDatasetPath('egg') in command line and run it. This will 
download the Egg data set from website and place it in examples/data.




